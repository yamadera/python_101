# ---------------------------------------------------------------------------------------- #
# One acre of land is equivalent to 43,560 square feet. Write a program that asks the user #
# to enter the total square feet in a tract of land and calculates the number of acres in  #
# the tract.                                                                               #
# Hint: Divide the amount entered by 43,560 to get the number of acres.                    #
# ---------------------------------------------------------------------------------------- #

# define ACRE 
ACRE = 43560

# Getting input from the user
user_input = input("Please enter the total square feet: ")

# Converting input to a floating-point number
try:
    acre = float(user_input) * ACRE
    print(f"{user_input} sq ft = {acre:.2f} acre(s)")
except ValueError:
    print("Invalid input. Please enter a valid number.")
