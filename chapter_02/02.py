# --------------------------------------------------------------------------------------- #
# A company has determined that its annual profit is typically 23 percent of total sales. #
# Write a program that asks the user to enter the projected amount of total sales, then   #
# displays the profit that will be made from that amount.                                 #
# Hint: Use the value 0.23 to represent 23 percent.                                       #
# --------------------------------------------------------------------------------------- #

# define annual profit 
ANNUAL_PROFIT = 0.23

# Getting input from the user
user_input = input("Please enter your projected sales: ")

# Converting input to a floating-point number
try:
    projected_sales = float(user_input)
    print("Projected profit: ", projected_sales * ANNUAL_PROFIT)
except ValueError:
    print("Invalid input. Please enter a valid number.")
