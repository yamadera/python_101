# --------------------------------------------------------------------------------------- #
# A customer in a store is purchasing five items. Write a program that asks for the price #
# of each item, then displays the subtotal of the sale, the amount of sales tax, and the  #
# total.                                                                                  #
# Assume the sales tax is 7 percent.                                                      #
# --------------------------------------------------------------------------------------- #

# define sales tax
SALE_TAX = 0.07

# define function that asks for an integer value from the user
def get_integer_from_user(prompt="Enter an integer: "):
    while True:
        try:
            user_input = input(prompt)
            return int(user_input)
        except ValueError:
            print("Invalid input. Please enter an integer.")

# define function that asks for an integer value from the user
def get_float_from_user(prompt="Enter an float: "):
    while True:
        try:
            user_input = input(prompt)
            return float(user_input)
        except ValueError:
            print("Invalid input. Please enter a floating number.")

# Ask the user how many items have been purchased
items_count = get_integer_from_user("How many item(s) did you purchase? ")
subtotal = 0.0

for index, _ in enumerate(range(items_count)):
    # Ask for the sales price and quantity
    item_price = get_float_from_user(f"What's the price for item {index + 1}: $")
    item_quantity = get_integer_from_user(f"How many item {index + 1} did you purchase: (quantity) ")
    item_cost = item_price * item_quantity
    subtotal += item_cost
    #print(f"{index + 1} - ${item_cost}")
    print("")

tax = subtotal * SALE_TAX
total = subtotal + tax

print("--------------------")
print(f"Subtotal: ${subtotal:.2f}")
print(f"     tax: ${tax:.2f}")
print("====================")
print(f"   Total: ${total:.2f}")
print("====================")
    




